# Introduccion a k8s

Este curso fue desarrollado con:
* [reveal-md](https://github.com/webpro/reveal-md)
* [asdf](https://asdf-vm.com/) con plugins para:
  * [direnv](https://direnv.net/)
  * [node](https://github.com/asdf-vm/asdf-nodejs)

Por ello, si se tiene instalado todo lo anterior, simplemente ingresando al
directorio, debe correrse:

```
npm i
npm start
```
