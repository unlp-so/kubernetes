---
title: Sistemas Operativos - Introducción a kubernetes
theme: moon
highlightTheme: vs2015
preprocessor: preproc.js
revealOptions:
  transition: 'concave'
---
# Sistemas Operativos

## Introducción a kubernetes

![logo](static/info.png)<!-- .element: height="110px" -->

<div class="small">

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img
alt="Licencia Creative Commons" style="border-width:0"
src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />Esta obra
está bajo una <a rel="license"
href="http://creativecommons.org/licenses/by/4.0/">Licencia Creative Commons
Atribución 4.0 Internacional</a>.

</div>

---

## Agenda

* Introducción
* Tipos de objetos en k8s
* Un ejemplo

---

FILE: introduccion.md

---

FILE: objetos.md

---

FILE: ejemplo.md
