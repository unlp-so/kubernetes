# Introducción

  - historia
  - arquitectura
  - alta disponibilidad
  - instalaciones de kubernetes

----

**Kubernetes** es una plataforma portable y extensible de código abierto para
administrar cargas de trabajo y servicios. Kubernetes facilita la automatización
y la configuración declarativa. Tiene un ecosistema grande y en rápido
crecimiento.

<div class="small fragment">

El nombre Kubernetes o <b>K8S</b> surge del griego, significando
_timonel_ o _piloto_.
</div>

----

## Inicios de kubernetes

Google liberó el proyecto k8s en el año 2014. Kubernetes se basa en 
[Borg](https://ai.google/research/pubs/pub43438) un producto
propietario de Google utilizado para ejecutar aplicaciones en producción a gran
escala por más de una década y media.


----

## Línea del tiempo

<div class="box">

[![](https://mermaid.ink/img/pako:eNpNUMFKQzEQ_JVlTwo9VK3V7tEKXioKRZGSS8jb5gWS7GOTB0rpvzdW1Len3ZnZYZgDOukYCWtIHENmk-E8V6u7FYHrVaQCZ3jL4fOXu57PbwieRHxkeBD1E2JBsJVoNRRYS662WWqZ8LcEL0Pze99NwCXBq4rjUsD9PcGF8yrjUC4nynuCzcfaZJxhYk02dC374VtgsPac2CC1NQbfV4MmH5vQjlW2X9khVR15huPQ2cqPwXq1CWlvY2noYPNO5P_mLlTR5592ziUdT5bCXNA?type=png)](https://mermaid.live/edit#pako:eNpNUMFKQzEQ_JVlTwo9VK3V7tEKXioKRZGSS8jb5gWS7GOTB0rpvzdW1Len3ZnZYZgDOukYCWtIHENmk-E8V6u7FYHrVaQCZ3jL4fOXu57PbwieRHxkeBD1E2JBsJVoNRRYS662WWqZ8LcEL0Pze99NwCXBq4rjUsD9PcGF8yrjUC4nynuCzcfaZJxhYk02dC374VtgsPac2CC1NQbfV4MmH5vQjlW2X9khVR15huPQ2cqPwXq1CWlvY2noYPNO5P_mLlTR5592ziUdT5bCXNA)

</div>

----
<!-- .slide: data-auto-animate -->

## Línea del tiempo

<div class="box">

[![](https://mermaid.ink/img/pako:eNpVkU9LAzEQxb_KkJOFCv4Xc3SLsIhW2pvkMm5m3dDNZMkmQlv63Z2uVGNOkzdvfi9k9qoJlpRWyXnqHZNhmM7VxeW1hkVoNhQL7UbDc_6gyJRoLPRbDcuBGKrACQUToWaXHCb3RXC2rOpZYb47gaF3wkJoTlNWF4RV5uOrhJQottgIqFrVMw3rTRgoFMD7X6ANXOIAoXqtnv5b34L1yLCFx-x6i13RfSjyrTgk73wpPO-IdzKDMEpIJPJDj7sAliRxCh475wEMq7nyFD06K5-6P6KNSh15MkpLaanF3CejDB_EijmF9ZYbpVPMNFd5sJho4fAzole6xX4UdUB-D-HvTtalEF9-Fjft7_AN7jGQhw?type=png)](https://mermaid.live/edit#pako:eNpVkU9LAzEQxb_KkJOFCv4Xc3SLsIhW2pvkMm5m3dDNZMkmQlv63Z2uVGNOkzdvfi9k9qoJlpRWyXnqHZNhmM7VxeW1hkVoNhQL7UbDc_6gyJRoLPRbDcuBGKrACQUToWaXHCb3RXC2rOpZYb47gaF3wkJoTlNWF4RV5uOrhJQottgIqFrVMw3rTRgoFMD7X6ANXOIAoXqtnv5b34L1yLCFx-x6i13RfSjyrTgk73wpPO-IdzKDMEpIJPJDj7sAliRxCh475wEMq7nyFD06K5-6P6KNSh15MkpLaanF3CejDB_EijmF9ZYbpVPMNFd5sJho4fAzole6xX4UdUB-D-HvTtalEF9-Fjft7_AN7jGQhw)

</div>


----

## Luego de k8s 1.0

El crecimiento de kubernetes desde 2014 en adelante fue exponencial.

[El 20 de agosto de 2018](https://www.cncf.io/announcement/2018/08/29/cncf-receives-9-million-cloud-credit-grant-from-google)
Google transfiere la propiedad de k8s a la fundación [CNCF](https://www.cncf.io/),
además de 9 millones de u$s en créditos en GCE para cubrir costos de
infraestructura asociados al desarrollo y distribución de k8s.

----
## ¿Qué es CNCF?

CNCF significa _Cloud Native Computing Fundation_.

La [definición](https://github.com/cncf/toc/blob/master/DEFINITION.md#espa%C3%B1ol)
de Cloud Native dice: 
<div class="small fragment" >

Las tecnologías CN empoderan a las organizaciones para construir y correr
aplicaciones escalables en ambientes heterogéneos (nubes públicas, privadas
o hibridas). Temas como contenedores, service mesh, microservicios,
infraestructura inmutable y APIs declarativas son ejemplos de este enfoque.
</div>
<div class="small fragment">

Estas técnicas permiten crear sistemas de bajo acoplamiento que son resilientes,
administrables y observables, simplificando además la aplicación de cambios de
alto impacto de manera frecuente y predecible.
</div>
<div class="small fragment">

La CNCF busca impulsar la adopción de este paradigma mediante el fomento y
mantenimiento de un ecosistema de proyectos de código abierto y neutro con
respecto a los proveedores.
</div>

----

## Productos CNCF

<small>
La CNCF propone seguir el <a href="https://raw.githubusercontent.com/cncf/trailmap/master/CNCF_TrailMap_latest.pdf"> siguiente camino de adopción de productos</a>
</small>

<div class="container">
  <div class="col">

  Productos
  [<img alt="CNCF Landscape" src="static/cncf-landscape.png"
/>](https://landscape.cncf.io)
  </div>

  <div class="col">

  Miembros
  [<img alt="CNCF Landscape" src="static/cncf-members.png" />](https://landscape.cncf.io/members)
  </div>
</div>

----
<!-- .slide: data-auto-animate  -->

## Criterio de graduación CNCF

El criterio está basado en _**Crossing the chasm** -Cruzando el abismo-_: un
[libro de Geoffrey A Moore](http://www.geoffreyamoore.com/). Este criterio
estipula las dificultades que atraviesan los productos tecnológicos para ser
adoptados. Dice que existe un abismo entre los productos que son adoptados por los
entusiastas y visionarios (early adopters) y los pragmáticos (las grandes
mayorías).


----
<!-- .slide: data-auto-animate  -->
## Criterio de graduación CNCF

<div class="box">

<img src="static/cncf-graduation.svg" />

</div>
----
<!-- .slide: data-auto-animate  -->
## Criterio de graduación CNCF

<div class="small">

Los proyectos aspiran a estar como [graduated o
incubating](https://www.cncf.io/projects/). Para ello deben cumplir:

* **Sandbox Stage:** reqiuere sponsors,  respetar el [código de conducta de la
    CNCF](https://github.com/cncf/foundation/blob/master/code-of-conduct-languages/es.md),
    adherir a la [política de propiedad intelectual](https://github.com/cncf/foundation/blob/master/charter.md#11-ip-policy)
    y mostrar su estado sandbox prominentemente en su sitio.
* **Incubating Stage:** igual que sandbox pero garantizando que al menos tres
  usuarios independientes lo utilicen en producción, tener una cantidad de
  contribuciones garantizadas, un esquema de versionado claro.
* **Graduated Stage:** igual que incubating, pero además que al menos dos de sus
  desarrolladores pertenezcan a organizaciones diferentes, mantener un listado
  público de implementaciones de referencia,  manterner una [Core
  Infraestructure Initiative](https://www.coreinfrastructure.org/), dejar
  explícita la gobernanza del proyecto y el proceso de aceptación de
  contribuciones, y recibir la mayoría de votos del comité.

</div>

----

# Arquitectura
----

## ¿Qué es un cluster?

Es un conjunto de nodos que se comportan como un único sistema, 
ofreciendoofreciendo de esta forma
redundancia y alta disponibilidad.
<!-- .element: class="fragment" data-fragment-index="1" -->

A partir de ahora, entenderemos como nodo del cluster a cualquier máquina,
física o virtual, que se encuentre corriendo los servicios de k8s y se
haya registrado en el cluster.
<!-- .element: class="fragment" data-fragment-index="2" -->

El cluster persiste su estado en una base de datos distribuída, razón por la que
debe contemplar los problemas planteados por el [teorema de CAP](https://en.wikipedia.org/wiki/CAP_theorem).
<!-- .element: class="fragment" data-fragment-index="3" -->

----

## Que es Kubernetes

Es un sistema de código abierto para automatizar el
despliegue, escalamiento y administración de aplicaciones conteinerizadas y
servicios.

----

<!-- .slide: class="list medium" -->
## Que no es Kubernetes

<div class="small">

- No es una plataforma como servicio.
  <!-- .element: class="fragment" data-fragment-index="1" -->
  - simplemente provee una capa de abstracción al hardware para interactuar
    con aplicaciones en contenedores.
  <!-- .element: class="fragment" data-fragment-index="1" -->
- No construye ni despliega el código (CI/CD), aunque puede integrarse
  fácilmente.
  <!-- .element: class="fragment" data-fragment-index="2" -->
- No provee servicios a nivel de capa de aplicación.
  <!-- .element: class="fragment" data-fragment-index="3" -->
- No provee servicios dedicados al monitoreo, alertado o captación
  de logs.
  <!-- .element: class="fragment" data-fragment-index="4" -->
- No provee un lenguaje propio para la configuración. Esto se hace mediante JSON
  o YAML.
  <!-- .element: class="fragment" data-fragment-index="5" -->
- Es mucho más que un orquestador de contenedores: 
  <!-- .element: class="fragment" data-fragment-index="6" -->
  _la definición de
  orquestación es la ejecución de un flujo definido: primero
  ejecutar A, luego B luego C. K8S por su parte provee diferentes procesos de
  control que se componen y actúan de forma independiente para llevar el estado
  actual al deseado._
  <!-- .element: class="fragment" data-fragment-index="7" -->

</div>

Notes:
- Un ejemplo de paas es OpenShift, que funciona montando una capa más de
  abstracción sobre k8s
- En principio, k8s no ofrece integración directa para CI/CD, como si lo hace
  OpenShift
- No ofrece middleware, framework de procesamiento de datos, bases de datos, etc
- Puede configurarse fácilmente monitoreo usando prometheus y las métricas
  exportadas nativamente por el cluster, pero nada es nativo.
- Se dice que k8s elimina la necesidad de orquestar, ya que la orquestación se
  define como la ejecución de un flujo de trabajo estricto, mientras que k8s
  está compuesto de un conjunto de procesos de control independientes y combinables
  entre si que llevan el estado actual hacia el estado deseado

----

## Arquitectura base

Dentro de un cluster de k8s, se pueden distinguir los siguientes roles:

<div class="container">
  <div class="col">

  * master
  * worker
  * etcd
  </div>
  <div class="col">

  <img alt="Arquitectura de k8s" src="static/k8s-architecture.png" />
  </div>

</div>

<small class="fragment" data-fragment-index="1">
<em>Un mismo nodo puede cumplir más de un rol al mismo tiempo</em>.

Todos los componentes se comunican **usando APIs siguiendo el estandar
[OpenAPI](https://www.openapis.org/)**, usando https y [gRPC](https://grpc.io/).
Su diseño es de micro servicios.
</small>

----
## Servicios en 
# nodos master
----

<!-- .slide: data-auto-animate  -->
## Nodos master

_**Cluster Control Plane:** toma decisiones globales_

- **kube-apiserver**
  - Exponen la API de k8s.
  - Es el frontend del cluster.
  - Diseñado para escalar horizontalmente
  - Los comandos interactúan con él para realizar las operaciones.
----
<!-- .slide: data-auto-animate  -->
## Nodos master

_**Cluster Control Plane:** toma decisiones globales_

- **kube-scheduler**
  - Observa la creación de nuevos [pods](https://kubernetes.io/es/docs/concepts/workloads/pods/pod/) que aún no se les ha asignado un nodo,
    seleccionando un nodo para su ejecución.
  - Los factores para definir qué nodo elegir incluyen requerimientos de
    recursos, restricciones de hardware, sofware y políticas, especificaciones
    de (no)afinidad, localidad de datos.
----
<!-- .slide: data-auto-animate  -->
## Nodos master

_**Cluster Control Plane:** toma decisiones globales_

- **kube-controller-manager:** es un binario con varios controladores

  - **Node controller:** responsable de anoticiar cuando un nodo 
    deja de responder.
  - **Replica controller:** mantiene el número correcto de pods para cada
    objeto de replicación en el sistema.

----
<!-- .slide: data-auto-animate  -->
## Nodos master

_**Cluster Control Plane:** toma decisiones globales_

- **kube-controller-manager:** es un binario con varios controladores

  - **Endpoints controller:** despliega los objetos endpoint (une servicios
    con pods).
  - **Service account & Token controllers:** crea cuentas por defecto y tokens
    de acceso a la API para los nuevos namespaces.

----
<!-- .slide: data-auto-animate  -->
## Nodos master

_**Cluster Control Plane:** toma decisiones globales_

- **cloud-controller-manager:** encargado de embeber los controles necesarios
  para interactuar con los servicios del proveedor de cloud subyacente:
  - **Node controller:** determina si un nodo ha sido eliminado cuando deja de
    responder.
  - **Route controller:** configura rutas en la red.
  - **Service controller:** configura load balancers.
  - **Volume controller:** crea, asocia y monta volúmenes.

----
## Servicios en 
# todos los nodos
----
<!-- .slide: data-auto-animate  -->
## Todos los nodos

Los siguientes servicios corren tanto en nodos master como en workers, _también
llamados **minion**_:

- **kubelet**
  - Asegura que los contenedores estén corriendo en un pod.
  - Toma los PodSpec que son provistos por diversos mecanismos, y asegura que
    los contenedores descriptos en dichos PodSpecs se encuentren corriendo y
    su estado sea _healthy_.
  - _No gestiona contenedores que no son creados por k8s_.
----
<!-- .slide: data-auto-animate  -->
## Todos los nodos

Los siguientes servicios corren tanto en nodos master como en workers, _también
llamados **minion**_:
- **kube-proxy**
  - Es un proxy de red que implementa parte del concepto de k8s llamado
    _Service_.
  - Mantiene reglas de red en los nodos, permitiendo así la comunicación con los
    pods correctos desde dentro y fuera del cluster.
  - Utiliza el soporte del sistema operativo de filtrado de paquetes si es que
    está disponible.

----
## Servicios en 
# nodos etcd
----
## Nodos etcd

Son los encargados de correr el servicio de etcd, una base de datos clave-valor
distribuida.
<!-- .element: class="fragment" data-fragment-index="1" -->

Este servicio almacena toda la información y el estado del cluster.
<!-- .element: class="fragment" data-fragment-index="2" -->

Para cumplir con el teorema de CAP implementan el algoritmo de [Raft](https://thesecretlivesofdata.com/raft/)
y por ello, el número de nodos será impar para lograr el consenso de `(N-1)/2`.
<!-- .element: class="fragment" data-fragment-index="3" -->

----

## La arquitectura completa

![Arquitectura de k8s](static/k8s-architecture.png)

----
<!-- .slide: data-auto-animate  -->

## Container runtimes

Para correr contenedores en pods, Kubernetes utiliza un container runtime
especifico. Algunos de ellos:


* [~docker~](https://www.docker.com/)
* [cri-o](https://cri-o.io/)
* [containerd](https://containerd.io/)

Notes: Kubernetes soporta CRI(Container Runtime Interface). Esta actua como un puente entre Kubelet y el container runtime que se quiera utilizar. Ademas, mencionar que CRI-O es una alternativa liviana en lugar de utilizar Docker como runtime para Kubernetes. 

----

<!-- .slide: data-auto-animate  -->

## Container runtimes 

[![CNCF container
runtimes](static/container-runtimes.png)](https://landscape.cncf.io/guide#runtime--container-runtime)

----
<!-- .slide: data-auto-animate  -->

## Container runtimes

* **Docker fue el estándar de facto por mucho tiempo:** hasta la versión 1.11, el
  daemon de docker estaba a cargo de la gestión de imágenes, lanzar
  contenedores y proveer una API para interactuar con el daemon. Todo corriendo
  como root.
* En 2015 docker junto a otras empresas lanzan [OCI](https://www.opencontainers.org/) _-Open Container Inintiative-_.
----
<!-- .slide: data-auto-animate  -->

## Container runtimes

* OCI define dos especificaciones:
  * **Runtime Spec:** con [runC](https://github.com/opencontainers/runc) como
    referencia de implementación, corre un OCI container sin pasar por docker
    engine. runC es un runtime de bajo nivel que no provee una API (como docker
    engine).
  * **Image Spec:** define un estandar para la construcción y transporte de imágenes.


----

<!-- .slide: data-auto-animate  -->

## Container runtimes

* **[containerd](https://containerd.io/):** para ofrecer aun más modularidad, docker
  introduce containerd, dando lugar a nuevos actores. Actúa como un API facade
  del container runtime (runc en este caso).
  * No admite construcción de imágenes.
* K8S por su parte, a partir de la versión 1.5 introduce [CRI](https://github.com/kubernetes/cri-api/) _-Container Runtime Interface-_,
  permitiendo a kubelet utilizar una gran variedad de container runtimes.

----
<!-- .slide: data-auto-animate  -->

## Container runtimes

![Sample containerd docker k8s](static/containerd-as-container-runtime.png)<!-- .element: height="400px" -->

<div class="small">

k8s, containerd y docker
</div>

----
<!-- .slide: data-auto-animate  -->

## Container runtimes

### CRI-O

<div class="container">
  <div class="col">

  <img src="static/crio-runtime.png" />
  </div>
  <div class="col small">

  * Container runtime liviano de alto nivel.
  * Creado específicamente para k8s.
  * Soporta runtimes de bajo nivel como runC.
  * Troubleshooting con [**`crictl`**](https://github.com/kubernetes-sigs/cri-tools/blob/master/docs/crictl.md).

</div>

----
<!-- .slide: data-auto-animate  -->

## Container runtimes

### Más herramientas independientes de docker

* [podman](https://podman.io/): maneja pods y contenedores sin utilizar un
  engine de contenedores.
* [buildah](https://buildah.io/): construye, descarga y sube imágenes a
  registries.
* [skopeo](https://github.com/containers/skopeo): descarga, sube, inspecciona y
  elimina imágenes de registries.
* [kaniko](https://github.com/GoogleContainerTools/kaniko): construye imágenes
  de contenedores y admite su publicación.

----

## Redes: desafío


- Nodos que forman parte de un cluster pueden estar distribuidos.
- Nodos en redes IP diferentes.
<!-- .element: class="fragment" -->

<div class="small fragment" >

Los pods deben poder conectarse unos con otros, incluso cuando ejecutan en
diferentes nodos. Para ello, el cluster debe asignarles una IP de una red
(de qué red dependerá de la implementación que se use).
</div>
<div class="small fragment" >

La idea es utilizar tecnologías del tipo VXLAN _-Virtual Extensible LAN-_ para
encapsulamiento de capa 2, encapsulamiento de capa 3 de tipo IPIP, GRE o VXLAN,
y distribución de rutas de capa 3 usando protocolos como BGP u OSPF.
</div>

----

### Networks plugins

Los más comunes son los siguientes:

- [Flannel](https://coreos.com/flannel/)
- [Calico](https://www.projectcalico.org/)
- [Canal](https://docs.projectcalico.org/latest/getting-started/kubernetes/installation/flannel): Calico + Flannel
- [Cilium](https://cilium.io/): basado en [eBPF](https://ebpf.io/)
- [Y muchos más](https://kubernetes.io/docs/concepts/cluster-administration/networking/)

<div class="small">

No solamente proveen conectividad entre pods en diferentes nodos usando
diferentes estrategias, sino que además algunos implementan [**Network Policies**](https://kubernetes.io/docs/concepts/services-networking/network-policies/)
que permiten limitar tráfico entrante o saliente de los pods
</div>


Notes: Kubernetes puede ser extendido agregando una variedad de addons.
En particular se pueden instalar diversos plugins de red para definir distintas politicas y configurar las redes dentro del cluster.

----
# Alta disponibilidad

----

## ¿Qué es HA?

Una propiedad deseable de un cluster k8s es que provea HA. Para asegurarlo es
necesario contar con al menos el siguiente inventario de nodos:

<div class="fragment">

- 3 nodos que cumplan el rol de **etcd**.
- 3 nodo con rol master, que podrían ser los mismos que etcd.
- 2 nodos con rol worker.
</div>

----

## Capacidad de kubernetes

En la versión 1.30 de k8s se asume:

* No más de 5.000 nodos.
* No más de 150.000 pods.
* No más de 300.000 de contenedores.
* No más de 110 pods por nodo.

----

# Instalación de Kubernetes
----

## Tipos de instalaciones

- **On-premise o Baremetal:** en este escenario se gestionan todos los nodos:
  master, etcd y workers. Además se utiliza el cluster para despliegue de
  aplicaciones.
- **Cloud providers:** puede que se use de dos formas:
  - Como On-premise pero usando servicios de la nube, e incluso aprovechando
    capacidades de escalamiento de workers.
  - **Managed kubernetes:** solo se elige el tipo de máquinas empleados como workers
    y los masters y etcd son gestionados por el proveedor de cloud.

----
<!-- .slide: data-auto-animate  -->
## Instalación On-premise

- **[kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/):**
 provee acciones para administrar un cluster
  - `kubeadm init`
  - `kubeadm join`
  - `kubeadm destroy`
- **[kubespray](https://github.com/kubernetes-sigs/kubespray):** playbooks de [ansible](https://www.ansible.com/).
 Sirve para on-premise y AWS.
----
<!-- .slide: data-auto-animate  -->
## Instalación On-premise

- **[k3s](https://k3s.io/):** versión minimalista, creado para IoT y edge
  computing. Corre en HW de pocos recursos como RPI.
- **[k0s](https://k0sproject.io/):** versión minimalista, similar a k3s.
- **[Metal kubed](http://metal3.io/):** utiliza el concepto de [Cluster
  API](https://github.com/kubernetes-sigs/cluster-api) para instalar kuberntes.
----
## Instalación para dev / desktop
 - **[Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/):**
   despliega un cluster de un nodo en una VM.
 - **[Microk8s](https://microk8s.io/):** despliega un cluster de un nodo en una VM.
   Es posible configurar más de un nodo. Ideal para trabajo local y desarrollo.
 - **[kind](https://kind.sigs.k8s.io/):** kubernetes in docker.

----

## Cloud providers: managed k8s

- Google Cloud: [GKS](https://cloud.google.com/kubernetes-engine/)
- AWS: [EKS](https://aws.amazon.com/es/eks/)
- Azure: [AKS](https://azure.microsoft.com/es-es/services/kubernetes-service/)

----

### Cloud providers

[![Cloud providers k8s](static/cloud-providers.png)<!-- .element: height="500px"
-->](https://landscape.cncf.io/category=certified-kubernetes-hosted&format=card-mode&grouping=category)

----
## Veamos un ejemplo con kind

[kind](https://kind.sigs.k8s.io/) se instala con un binario, y simplemente
podemos crear un cluster usando:

```
kind create cluster --config CONFIG-KIND.yaml --name k8s-intro
```

> Es importante destacar que puede incluso usarse `--kubeconfig` para indicar
> dónde se creará la configuración de `kubectl`. Si no se especifica se usará la
> ubicación por defecto.

----
<!-- .slide: data-auto-animate  -->

## Creamos un cluster con kind

Primero la configuración: 

```bash
mkdir -p ~/so/kubernetes/.kube
cat > ~/so/kubernetes/.kube/kind-config.yaml <<EOF
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
  - role: control-plane
    image: kindest/node:v1.26.6
    kubeadmConfigPatches:
    # Agrega al control plane un label que usaremos para que
    # el ingress controller corra en este nodo
      - |
        kind: InitConfiguration
        nodeRegistration:
          kubeletExtraArgs:
            node-labels: "ingress-ready=true"
    extraPortMappings:
    # Configura puertos mapeados localmente hacia el cluster
    # Se exponen los puertos 80 y 443 para los ejemplos con ingress.
    # El puerto 30300 es solamente usado en el primer ejemplo a modo
    # liustrativo
      - containerPort: 80
        hostPort: 80
        protocol: TCP
      - containerPort: 443
        hostPort: 443
        protocol: TCP
      - containerPort: 30300
        hostPort: 30300
  - role: worker
    image: kindest/node:v1.26.6
  - role: worker
    image: kindest/node:v1.26.6
EOF
```
----
<!-- .slide: data-auto-animate  -->

## Creamos un cluster con kind

Ahora creamos el cluster:

```
# Util si se usa direnv
echo 'export KUBECONFIG=$PWD/.kube/config' \
  > ~/so/kubernetes/.envrc

kind --kubeconfig ~/so/kubernetes/.kube/config \
  create cluster \
  --config ~/so/kubernetes/.kube/kind-config.yaml \
  --name k8s-intro
```

> Notar que usamos `--kubeconfig`. Si no se usa podemos pisar otra configuración
> de kubernetes en nuestra PC. Se recomienda usar la variable de ambiente
> `KUBECONFIG` para evitar así setear este parámetro.
<!-- .element: class="fragment" -->
