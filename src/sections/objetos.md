# Objetos de kubernetes
----
## Mapa de objetos

<div class="box" >

[![](https://mermaid.ink/img/pako:eNp1kcFOhTAQRX-FdFUS_AG2sNHExEjiQnEx0gFG2w4pgwkh_LvFxxNidFa9t-e2086iGjaocuXIGwdD7ZMkMIvWA5s03WSSLEvBXoA8hnW9WFtV2ASUQ-c5Nex1C0kLNx84p8dWzLfU7ef_Qbdk8YQ_YBhpFPTyxHZy-E-qN-YU2kr_ThYWyO3QyyMOlhoYUV6vMV1Gj2cXAzulKwHBdrIRu1oloGN_Mu747bhZF4H9-4-TVhg-qUF9tJbe-i7gOEZLZcphcEAmfvqyIbWSHuMbVR6XlrpealX7NYIwCVezb1QuYcJMTYOJrZUEXQCn8hZij5kawD8zHxoNCYf7y1i_p7t-ARCglVg?type=png)](https://mermaid.live/edit#pako:eNp1kcFOhTAQRX-FdFUS_AG2sNHExEjiQnEx0gFG2w4pgwkh_LvFxxNidFa9t-e2086iGjaocuXIGwdD7ZMkMIvWA5s03WSSLEvBXoA8hnW9WFtV2ASUQ-c5Nex1C0kLNx84p8dWzLfU7ef_Qbdk8YQ_YBhpFPTyxHZy-E-qN-YU2kr_ThYWyO3QyyMOlhoYUV6vMV1Gj2cXAzulKwHBdrIRu1oloGN_Mu747bhZF4H9-4-TVhg-qUF9tJbe-i7gOEZLZcphcEAmfvqyIbWSHuMbVR6XlrpealX7NYIwCVezb1QuYcJMTYOJrZUEXQCn8hZij5kawD8zHxoNCYf7y1i_p7t-ARCglVg)

</div>

----
## Qué es un objeto de k8s

* Son entidades persistentes dentro del sistema k8s.
* Kubernetes utiliza estas entidades para representar el estado del cluster.
* Estas entidades describen:
  * Qué aplicaciones conteneirizadas corren, y en qué nodo.
  * Los recursos disponibles para estas aplicaciones.
  * Las políticas acerca de cómo esas aplicaciones se comportan: _políticas de
    reinicio, actualizaciones, y tolerancia a fallas._

----
## Formato de los objetos

Los objetos se describen como JSON o YAML, y por **cómo se estructuran ambos
formatos**. Un objeto consiste de secciones:

- **`apiVersion`**
- **`kind`**
- **`metadata`**
- **`spec`**

----
<!-- .slide: data-auto-animate  -->
## La API de k8s

* K8s fue diseñado considerando que todo producto de software debe crecer y
cambiar.
* La API de k8s se actualiza continuamente, por ello se versiona para así poder
agregar, eliminar o reestructurar objetos.
* El versionado de la API permite que un cluster k8s mantenga objetos con formatos diferentes.

<div class="fragment small">

_De esta forma se simplifican las actualizaciones del cluster._
</div>

----
<!-- .slide: data-auto-animate  -->
## La API de k8s

Veamos la API usando kubernetes para desplegar swagger-ui:

Primero exponemos la API de kubernetes como servicio:
<!-- .element: class="small" -->

  ```
  kubectl proxy -p 8000
  ```
----
<!-- .slide: data-auto-animate  -->
## La API de k8s

Veamos la API usando kubernetes para desplegar swagger-ui:

Ahora desplegamos swagger-ui exponemos con un servicio:
<!-- .element: class="small" -->

  ```
  kubectl run swagger \
    --env SWAGGER_JSON_URL=http://localhost:8000/openapi/v2 \
    --wait \
    --image swaggerapi/swagger-ui
  kubectl expose pod swagger --port 8080
  ```
  > Creará un pod llamado swagger, con la imagen `swaggerapi/swagger-ui`

----
<!-- .slide: data-auto-animate  -->
## La API de k8s

Veamos la API usando kubernetes para desplegar swagger-ui:

<div class="small">

Conectamos a la UI de swagger, usando una URL un tanto fea porque estamos
accediendo a un servicio a través del proxy que creamos en nuestro puerto 8000:

http://localhost:8000/api/v1/namespaces/default/services/swagger/proxy/

> Analizando la URL accedemos a una API rest que proxea los requerimientos del
> **namespace** llamado **default**, el **servicio** llamado **swagger**.

</div>

----
<!-- .slide: data-auto-animate  -->
## La API de k8s

Observando swagger, podemos inferir:

* Los objetos se agrupan: core, core_v1, apis, etc.
* Algunos grupos tiene una versión o más.
* Se ven métodos GET, POST, PUT, PATCH, DELETE.
* La mayoría de los endpoints requieren autorización.

----

## Interactuando con la API

Toda la interacción se realizará usando https. El cliente propio de kubernetes
es [**`kubectl`**](https://kubernetes.io/docs/tasks/tools/).

**`kubectl`** ofrece autocomplete para varios shells y su uso puede en un
principio parecer avasallante, pero luego es muy cómodo de usar.

<div class="container">

<div class="col">

Podremos ver todos los tipos de objetos:
<!-- .element: class="small" -->

```
kubectl api-resources
```

</div>
<div class="col">

Y todas las versiones soportadas:
<!-- .element: class="small" -->

```
kubectl api-versions
```

</div>
</div>
----
<!-- .slide: data-auto-animate  -->

## Analizando un objeto

```
kubectl get namespaces
kubectl get namespaces -o json
kubectl get namespaces default -o yaml
kubectl describe namespace default
```

----
<!-- .slide: data-auto-animate  -->

## Analizando un objeto

Inferimos que todo objeto tiene:

* kind
* apiVersion
* metadata
  * name
  * labels
  * annotations
  * uid

En base a los campos anteriores, específicamente del **kind** y **apiVersion**
dependerá la morfología de un objeto. La misma estará en el campo **spec**
o **data**, depediendo de cada objeto.
<!-- .element: class="fragment small" -->

----
## Labels y Anotaciones

En kubernetes los labels cumplen un rol muy especial: _**son utilizados para
buscar objetos que machean con los labels para realizar autodiscovery**_. Un
ejemplo, es el caso de los objetos **Service** que utilizan labels para saber
entre qué pods balancear tráfico.

Las anotaciones por su parte, son usadas para almacenar metadatos, pero no es
posible buscar por este campo.

----
## Namespace

En kubernetes, veremos que los objetos pueden ser globales o crearse en un
namespace.

Un namespace es una forma de separar objetos para agruparlos y aplicar políticas
de seguridad a cada uno de forma independiente. Un ejemplo de un objeto global,
es el **namespace**.

> Los objetos que mencionaremos a continuación, serán mayormente creados en
> un namespace.
----
<!-- .slide: data-auto-animate  -->
## Objetos más relevantes

* **Pod:** representa un workload. Es la unidad mínima de ejecución en k8s. Si
  bien podemos crear pods de forma imperativa, no es lo recomendado. Un pod
  podrá tener uno **o más contenedores**.
* **Deployment:** controla el despliegue de pods que serán creados y
  actualizados según estrategias de despliegue como RollingUpdate. Se dice que
  es controlado el despliegue porque tiene en cuenta muchos aspectos que evitan
  downtime de servicios. Utilizado en servicios stateless. Pueden escalar
  fácilemente los pods.
----
<!-- .slide: data-auto-animate  -->

## Objetos más relevantes

* **Statefulset:** similar a deployments pero para workloads que sean stateful,
  como son bases de datos, clusters, etc. También pueden fácilmente escalar
  pods.
* **Daemonset:** garantiza que un pod corra en cada nodo del cluster.
* **Job:** Corren tareas que finalizan, pudiendo correrlas en paralelo.
* **Cronjob:** corren jobs con cierta periodicidad y recurrencia.

----
## Objetos que permiten configurar contenedores

* **Configmap:** repositorio de valores en formato clave valor. Muy utilizado
  como complemento de configuración de contenedores en un pod.
* **Secrets:** similar al configmap, pero con la diferencia que los datos se
  codifican en base64 y es posible aplicar permisos diferentes al de los
  configmaps.

----

## Conectividad entre contenedores

* **Service:** un servicio buscará pods usando labels y de esta forma balancear
  el tráfico que ingrese al pod.
* **Ingress:** permtien ingresar tráfico al cluster usando un balanceador HTTP.

----

## Persistencia

* **PersistentVolumes:** representan un volúmen persistente a ser montado luego
  por contenedores en un pod. Este es un recurso **global**. Existen [múltiples
  tipos de volúmenes soportados](https://kubernetes-csi.github.io/docs/drivers.html)
* **PersistentVolumeClaims:** son una solicitud de un PV. Estos objetos sí
  pertenecen a un namespace.

