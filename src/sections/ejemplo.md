# Ejemplo de escalabilidad
## en kubernetes

----

## El ejemplo

El ejemplo a mostrar, considera la instalación de:

* Una base de datos [Time
  Series](https://en.wikipedia.org/wiki/Time_series_database), usando
[prometheus](https://prometheus.io/).
* Una UI que permite visualizar en dashboards lo que almacena prometheus.
  Usaremos [grafana](https://grafana.com/).
* Escalaremos en base a las métricas tomadas con prometheus, usando
  [Keda](https://keda.sh/)

> Todo el ejemplo, puede descargarse desde [este
> repositorio](https://github.com/Mikroways/k8s-keda-prometheus-demo).
